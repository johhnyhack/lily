const discordjs = require("discord.js")
const config = require(__dirname + "/" + "config.json")

const client = new discordjs.Client()

function roll(min, max) // min and max included
{
  return Math.floor(Math.random() * (max - min + 1) + min)
}

client.on("ready", async () => {
  await console.log(client.user.tag + " " + "is now ready.")
})

client.on("message", async (msg) => {
  let logs = client.guilds.get("518943823888515072").channels.get("588537565477601281")

  if (!msg.content.startsWith(config.prefix)) return
  let args = msg.content.substring(config.prefix.length).split(" ")

  switch (args[0]) {

    case "roll":
      if (args[1])
        msg.channel.send(roll(1, parseInt(args[1])))
      else
          msg.channel.send(roll(1, 6))
      break

    case 'kick':
      if (!msg.member.hasPermission('KICK_MEMBERS')) return msg.channel.send({ color: 0x1E88E5, embed: { title: '🛡 You dont have enough permissions!' } })
      if (!args[1]) return
      let member_to_kick = msg.mentions.members.first()
      logs.send({
        embed: {
          color: 0xF44336,
          description: '👊 User ' + "`" + member_to_kick.user.username + "`" + ' has been kicked from ' + msg.guild.name + ' Discord.',
          footer: {
            icon_url: msg.guild.iconURL,
            text: "Invoked by: " + msg.author.username + " | " + new Date()
          }
        }
      })
      args.splice(0, 2)
      member_to_kick.kick(args.join(" ") + " " + "kick invoked by:" + " " + msg.author.username)
      break

    case 'ban':
      if (!msg.member.hasPermission('BAN_MEMBERS')) return msg.channel.send({ color: 0x1E88E5, embed: { title: '🛡 You dont have enough permissions!' } })
      if (!args[1]) return

      let member_to_ban = msg.mentions.members.first()
      logs.send({
        embed: {
          color: 0xF44336,
          description: "🚫 User " + "`" + member_to_ban.user.username + "`" + " has been banned from " + msg.guild.name + " Discord.",
          footer: {
            icon_url: msg.guild.iconURL,
            text: "Invoked by: " + msg.author.username + " | " + new Date()
          }
        }
      })
      args.splice(0, 2)
      member_to_ban.ban({ days: 0, reason: args.join(" ") + " " + "ban invoked by:" + " " + msg.author.username })
      break

    case 'purge':
      if (!args[1]) return
      if (!msg.member.hasPermission("MANAGE_MESSAGES")) return msg.channel.send({ color: 0x1E88E5, embed: { title: '🛡 You dont have enough permissions!' } })

      let messages_to_delete = parseInt(args[1])
      msg.channel.fetchMessages({ limit: 30 }).then(function (messages) {
        var msg_array = messages.array()
        msg_array.length = messages_to_delete + 1
        msg_array.map(ms => ms.delete().catch(console.error))
      })
      break

    case 'invite':
      msg.channel.send({
        embed: {
          thumbnail: {
            icon_url: msg.guild.iconURL
          },
          color: 0x673AB7,
          title: '🏠 Invite: https://discord.gg/3FmbRS7',
          description: 'Total guild members: ' + msg.guild.memberCount
        }
      })
      break

    case 'say':
      if (!msg.member.hasPermission('MANAGE_MESSAGES')) return msg.channel.send({ embed: { color: 0x1E88E5, title: '🛡 You dont have enough permissions!' } })
      args.shift()
      msg.channel.send(args.join(" "))
      break

    case 'asay':
      if (!msg.member.hasPermission('MANAGE_MESSAGES')) return msg.channel.send({ embed: { color: 0x1E88E5, title: '🛡 You dont have enough permissions!' } })
      args.shift()
      msg.delete()
      msg.channel.send(args.join(" "))
      break
  }

})

client.login(config.token)